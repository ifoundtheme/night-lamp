# night-lamp

Little home automation for the moon lamp

T2 authorized with Kelsey's Mac

## Ideas
* Save all of the IR codes from the moon lamp remote
* Have the moon turn itself on when it detects light changes
* Have the moon react to calls to the Hue interface
* Have the moon turn itself off after a certain amount of time (30m after hue lights?)
* Relay turn off the star lamp too?
* Fun mode changing on the moon?
* Web interface so can ditch the remote and just use a PWA?