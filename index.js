// Set up libraries and hardware
var tessel = require('tessel');
var infraredlib = require('ir-attx4');
var ambientlib = require('ambient-attx4');
var infrared = infraredlib.use(tessel.port['A']);
var ambient = ambientlib.use(tessel.port['B']);
const ir_library = require('./signal_library.json')

// Declare variables
const lightsOut = 0.011; // Dark room with lights out
const lightsUp = 0.012; // Brightness of room with the lights on
var moonCommand = ir_library.pumpkin;
var lastCommand;
var turnOffTimer;
var brightenTimer;

// Define behaviors
//In the morning
function morning(moonCommand) {
  // Cancel any unfulfilled behaviors from other commands
  clearTimeout(turnOffTimer)
  // Turn the moon on
  sendSignal(moonCommand)
  // In a little while, brighten the moon
  brightenTimer = setTimeout( function () {
    sendSignal(ir_library.brighter)
  }, 10*1000) // 10 seconds
}

// In the evening
function evening(moonCommand) {
  // Cancel any unfulfilled behaviors from other commands
  clearTimeout(brightenTimer)
  // Dim the moon
  sendSignal(ir_library.dimmer)
  // Wait to turn off the moon
  turnOffTimer = setTimeout( function () {
    sendSignal(moonCommand);
  },  30*60*1000) // 30 minutes
}

// Reliably interpret light conditions with rolling average
// Set up rolling average
const arrLength = 20; // Length of rolling average
var avgStart = (lightsOut + lightsUp)/2
var avgArray = []
for(var i = 0; i < arrLength; i++) {
  avgArray.push(avgStart);
}

// Averaging function
function average(inputArray) {
  var total = 0;
  for(var i = 0; i < inputArray.length; i++) {
    total += inputArray[i];
  }
  return total / inputArray.length;
}

// Initial condition of rolling average
var averageValue = average(avgArray);

// Interaction with sensors
// Wait for a light signal
ambient.on('ready', function () {
  console.log("Here we go...")
  setInterval( function () {
    // Check the light level
    ambient.getLightLevel( function(err, lightdata) {
      console.log("Light level:", lightdata.toFixed(8));
      var avgVal = rollingAverage(lightdata);
      console.log('avg', avgVal)
      if ( avgVal > lightsUp) {
        // In the morning, turn the moon on when light goes above lightsUp
        moonCommand = ir_library.on;
      } else if (avgVal < lightsOut) {
        // In the evening, turn the moon off when light goes below lightsOut
        moonCommand = ir_library.off;
      }
      processSignal(moonCommand);
    });
  }, 1000); // Readings happen every 1 second
});

// Rolling average
function rollingAverage(newVal) {
  avgArray.shift(); // Remove the oldest value
  avgArray.push(newVal); // Add the new value
  return average(avgArray); // Return the new average
}

// Determine how to handle the command
function processSignal(moonCommand) {
  // When the conditions change
  if (moonCommand !== lastCommand) {
    // In the evenings
    if (moonCommand == ir_library.off) {
      evening(moonCommand)
    } else {
      morning(moonCommand)
    }
    lastCommand = moonCommand;
  }
}

// Send an infrared signal
function sendSignal(moonCommand) {
  // Make a buffer of on/off durations (each duration is 16 bits)
  var powerBuffer = Buffer.from(moonCommand, 'hex')
  // Send the signal at 38 kHz
  infrared.sendRawSignal(38, powerBuffer, function(err) {
    if (err) {
      console.log("Unable to send signal: ", err);
    } else {
      console.log("Signal sent!");
    }
  });
}

// Log when infrared is ready
infrared.on('ready', function() {
  console.log("Connected to IR!");
});
