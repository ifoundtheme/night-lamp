var tessel = require('tessel');
var infraredlib = require('ir-attx4');
var infrared = infraredlib.use(tessel.port['A']);
var fs = require('fs');

// When we're connected
infrared.on('ready', function() {
  console.log("Connected to IR!");
});

// If we get data, print it out
infrared.on('data', function(data) {
  console.log("Received RX Data: ", data.toString('hex'));
  // You can now copy/paste the raw buffer into your signal library
});